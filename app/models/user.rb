class User < ActiveRecord::Base
  attr_accessible :email, :name, :selected
  attr_accessor :give_to
  def self.create_santa
    @givers = User.all
    @recievers = User.all

    @givers.each do |giver|
      not_assigned = true
      while(not_assigned)
        choice = rand(0..@recievers.length-1)
        if !giver.email.eql? @recievers[choice].email
          giver.give_to = @recievers[choice]
          @recievers.slice!(choice)
          not_assigned = false
        else
          # If the only name left in the list is the giver
          if @recievers.length == 1
            giver.give_to = @givers[0].give_to
            @givers[0].give_to = giver
            not_assigned = false
          end
        end
      end
    end

    @givers.each do |giver|
      email_text = "Hi #{giver.name}, You have to give a secret santa gift to #{giver.give_to.name}"
      UserMailer.santa_mail(giver, email_text).deliver
    end
    "Done"
  end

end
